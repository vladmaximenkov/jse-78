package ru.vmaksimenkov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Getter
@Setter
public class CustomUser extends User {

    @Nullable
    private String id;

    @Nullable
    private String email;

    public CustomUser(@NotNull final UserDetails user) {
        super(
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities()
        );
    }

    public CustomUser(@NotNull final String username, @NotNull final String password, @NotNull final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public CustomUser withUserId(@NotNull final String userId) {
        this.setId(userId);
        return this;
    }

}
