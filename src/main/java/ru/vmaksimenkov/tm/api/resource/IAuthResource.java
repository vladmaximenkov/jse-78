package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.vmaksimenkov.tm.model.Result;
import ru.vmaksimenkov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RestController
@RequestMapping("/api/auth")
public interface IAuthResource {

    @NotNull
    @WebMethod
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    Result login(
            @WebParam(name = "username") @RequestParam("username") String username,
            @WebParam(name = "password") @RequestParam("password") String password
    );

    @Nullable
    @WebMethod
    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    User profile();

    @NotNull
    @WebMethod
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    Result logout();

}
