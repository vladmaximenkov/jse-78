package ru.vmaksimenkov.tm.api.resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/projects")
public interface IProjectCollectionResource {

    @NotNull
    @WebMethod
    @GetMapping
    Collection<ProjectRecord> get();

    @WebMethod
    @PostMapping
    void post(@NotNull @WebParam(name = "projects") @RequestBody List<ProjectRecord> projects);

    @WebMethod
    @PutMapping
    void put(@NotNull @WebParam(name = "projects") @RequestBody List<ProjectRecord> projects);

    @DeleteMapping
    void delete();

}
