package ru.vmaksimenkov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.exception.RoleNotFoundException;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkRole;

@Getter
public enum RoleType {

    USER("UserRecord"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    RoleType(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static @NotNull RoleType getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

}
